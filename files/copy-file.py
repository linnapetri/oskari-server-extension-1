import sys
import json

from PIL import Image
import numpy

print("This is the name of the script: ", sys.argv[0])
print("Number of arguments: ", len(sys.argv))
print("The arguments are: " , str(sys.argv))

json_file = sys.argv[1]

print("json file is " + json_file)

with open(json_file, 'r') as f:
    contents = json.load(f)

print("contents ", contents);

input_file = contents["input_file"]
output_file = contents["output_file"];

#read image
img = Image.open(input_file)

#view the image

imarray = numpy.array(img)
imarray = numpy.invert(imarray)
img_dst = Image.fromarray(imarray)
img_dst.save(output_file)
#img_dst.show()
print(imarray.shape)
print(imarray.size)
