const reportMaker = require('./report-maker');
const dotenvDefaults = require('dotenv-defaults');
const path = require('path');
const fs = require('fs')

dotenvDefaults.config();

const rootPath = process.env.FARM_DATA_ROOT_FOLDER;

const folder = 'kansio';
const fullPath = path.join(rootPath, folder);

(async () => {
  const report = await reportMaker(fullPath);
  await fs.promises.writeFile('output.html', report);
})();

