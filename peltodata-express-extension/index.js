const express = require('express');
const reportMaker = require('./report-maker');
const dotenvDefaults = require('dotenv-defaults');
const path = require('path');
const app = express();
const port = 3000;

dotenvDefaults.config();

const rootPath = process.env.FARM_DATA_ROOT_FOLDER;

app.get('/:folder', async (req, res) => {
  const folder = req.params.folder;
  const fullPath = path.join(rootPath, folder);
  console.log(`${new Date().toISOString()}: requesting report for folder "${folder}" ${fullPath}`);
  const report = await reportMaker(fullPath);
  if (report == null) {
    console.log(`${new Date().toISOString()}: folder ${fullPath} missing or other error`);
    res.status(404)
    res.send();
  } else {
    res.send(report);
  }
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
