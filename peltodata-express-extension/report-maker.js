const fs = require('fs');
const path = require('path');
const _ = require('lodash');
const Handlebars = require('handlebars');
const mimetypes = require('mime-types');

const getFiles = async (folder) => {
  const onlyFiles = [];
  try {
    const folderExists = fs.existsSync(folder);
    if (!folderExists) {
      return [];
    }
    const files = await fs.promises.readdir(folder);

    for (const file of files) {
      const fullPath = path.join(folder, file);
      const stat = await fs.promises.stat(fullPath);

      if (stat.isFile())
        onlyFiles.push(file);
    }
  } catch (e) {
    console.log(e);
  }

  return onlyFiles;
}

const getFileContents = async (folder, f) => {
  const data = await fs.promises.readFile(path.join(folder, f), { encoding: 'base64' });
  const mime = mimetypes.lookup(f);
  return `data:${mime};base64, ${data}`;
}

const getDataObject = async (folder) => {
  const files = await getFiles(folder);

  const data = {
    farmFields: null,
    farmId: null,
    farmWeatherLong: null,
    farmWeatherYearly: [],
    farmWeatherTempsums: [],
    segments: [],
    orthos: [],
    lidars: [],
    soiltypes: [],
    cropAmount: null,
    cropCycle: null,
    soilDescription: null,
  }

  const cropAmount = /crops_amount_farmid_/; // tämä
  const cropCycle = /crops_cycle_farmid_/; // tämä
  const farmFields = /fields_farmid_([\d]+)\./;
  const lidar = /lidar_.+fieldid_([\d]+)\./
  const ortho = /ortho_([\d]+)_fieldid_([\d]+)\./;
  const segments = /segments_(.+)_([\d]+)_fieldid_([\d]+)\./;
  const soilDescription = /soil_description\./;
  const soiltype = /soiltype_fieldid_([\d]+)\./;
  const farmWeatherYearly = /weather_([\d]+)_farmid+/;
  const farmWeatherLong = /weather_long_farmid_([\d]+)\./;
  const farmWeatherTempsum = /weather_tempsum_([\d]+)_farmid/;

  // ortho_2019_fieldid_xxxxxx
  for (let i = 0; i < files.length; i++) {
    const f = files[i];
    const fileContents = await getFileContents(folder, f);
    if (farmFields.test(f)) {
      const farmId = f.match(farmFields)[1];
      data.farmFields = f;
      data.farmFieldsData = fileContents;
      data.farmId = farmId;
    } else if (farmWeatherLong.test(f)) {
      const farmId = f.match(farmWeatherLong)[1];
      data.farmWeatherLong = f;
      data.farmWeatherLongData = fileContents;
      data.farmId = farmId;
    } else if (farmWeatherTempsum.test(f)) {
      const matches = f.match(farmWeatherTempsum);
      const year = matches[1];
      data.farmWeatherTempsums.push({ file: f, year });
    } else if (farmWeatherYearly.test(f)) {
      const year = f.match(farmWeatherYearly)[1];
      data.farmWeatherYearly.push({ file: f, year, data: fileContents })
    } else if (segments.test(f)) {
      const matches = f.match(segments);
      const type = matches[1];
      const year = matches[2];
      const fieldId = matches[3];
      data.segments.push({ file: f, type, year, fieldId, data: fileContents });
    } else if (ortho.test(f)) {
      const matches = f.match(ortho);
      const year = matches[1];
      const fieldId = matches[2];
      data.orthos.push({ file: f, year, fieldId, data: fileContents });
    } else if (lidar.test(f)) {
      const matches = f.match(lidar);
      const fieldId = matches[1];
      data.lidars.push({ file: f, fieldId, data: fileContents })
    } else if (soiltype.test(f)) {
      const matches = f.match(soiltype);
      const fieldId = matches[1];
      data.soiltypes.push({ file: f, fieldId, data: fileContents })
    } else if (soilDescription.test(f)) {
      data.soilDescription = { file: f, data: fileContents }
    } else if (cropAmount.test(f)) {
      data.cropAmount = { file: f, data: fileContents }
    } else if (cropCycle.test(f)) {
      data.cropCycle = { file: f, data: fileContents }
    }
  }

  data.segments = _.sortBy(data.segments, ['fieldId', 'type', 'year',]);
  data.orthos = _.sortBy(data.orthos, ['fieldId', 'year',]);
  data.lidars = _.sortBy(data.lidars, ['fieldId', 'file',]);
  data.farmWeatherYearly = _.sortBy(data.farmWeatherYearly, ['fieldId', 'year',]);
  data.soiltypes = _.sortBy(data.soiltypes, ['fieldId']);

  return data;
}
// Kuvan nimeäminen: weather_2019_farmidxxxx.png<BR>
// (async () => {
//   const source = await fs.promises.readFile('./template.handlebars');
//   const template = Handlebars.compile(source.toString());
//   const data = await getDataObject(folder);
//   const result = template(data);
//   await fs.promises.writeFile('output.html', result);
// })();

const getReport = async (folder) => {
  const data = await getDataObject(folder);
  const source = await fs.promises.readFile('./template.handlebars');
  const template = Handlebars.compile(source.toString());
  const result = template(data);
  return result;
  // await fs.promises.writeFile('output.html', result);
}

module.exports = getReport;
// Loop through all the files in the temp directory
