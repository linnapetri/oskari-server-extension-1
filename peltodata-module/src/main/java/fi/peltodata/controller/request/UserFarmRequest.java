package fi.peltodata.controller.request;

import java.io.Serializable;

public class UserFarmRequest implements Serializable {
    private String farmId;

    public UserFarmRequest(String farmId) {
        this.farmId = farmId;
    }

    public UserFarmRequest() {
    }

    public String getFarmId() {
        return farmId;
    }

    public void setFarmId(String farmId) {
        this.farmId = farmId;
    }
}
