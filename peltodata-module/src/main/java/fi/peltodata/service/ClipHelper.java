package fi.peltodata.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fi.nls.oskari.domain.User;
import fi.nls.oskari.domain.map.OskariLayer;
import fi.nls.oskari.service.ServiceException;
import fi.peltodata.GeoJson;
import fi.peltodata.domain.Farmfield;
import fi.peltodata.domain.Peltolohko;
import org.apache.commons.lang.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public class ClipHelper {
    private PeltodataService peltodataService;

    public ClipHelper() throws ServiceException {
        peltodataService = new PeltodataServiceMybatisImpl();
    }

    public String fillParams(OskariLayer layer, User user) {
        if (clipDisabled(layer)) {
            return "";
        }

        List<Farmfield> farmfields = peltodataService.findAllFarmfieldsByUser(user.getId());
        if (farmfields.size() == 0 && user.isAdmin()) {
            return "";
        } else if (farmfields.size() == 0) {
            throw new RuntimeException("clipped map cannot be returned unless user has fields registered");
        }

        Optional<Farmfield> f = farmfields.stream().filter(ff -> ff.getFarmId() != null).findFirst();

        if (!f.isPresent()) {
            return "";
        }

        String farmId = f.get().getFarmId();

        List<Peltolohko> lohkot = peltodataService.getPeltolohkot(farmId);
        ObjectMapper om = new ObjectMapper();
        List<GeoJson> geoJsons = lohkot.stream().map(Peltolohko::getGeoJson).map(geo -> {
            try {
                return om.readValue(geo, GeoJson.class);
            } catch (JsonProcessingException e) {
                return null;
            }
        }).filter(Objects::nonNull).collect(Collectors.toList());

        return getClipParam(geoJsons);
    }

    public static String getClipParam(List<GeoJson> geoJsons) {
        List<String> clip = new ArrayList<>();
        // MULTIPOLYGON( ((FOO BAR,FOO BAR)),(()) )
        for (GeoJson geoJson : geoJsons) {
            if (geoJson.getUnwrappedCoordinates().size() == 0) {
                continue;
            }

            List<String> coordinates = new ArrayList<>();

            for (List<String> unwrappedCoordinate : geoJson.getUnwrappedCoordinates()) {
                coordinates.add(StringUtils.join(unwrappedCoordinate, " "));
            }

            String singleField = "((" + StringUtils.join(coordinates, ",") + "))";
            clip.add(singleField);
        }

        String fullClipParam = "MULTIPOLYGON(" + StringUtils.join(clip, ",") + ")";
        return "&clip=" + fullClipParam;
    }


    private boolean clipDisabled(OskariLayer layer) {
        JSONObject attributes = layer.getAttributes();
        if (attributes == null) {
            return true;
        }

        if (!attributes.has("clip_user_fields")) {
            return true;
        }
        boolean clipUserFields;
        try {
            clipUserFields = attributes.getBoolean("clip_user_fields");
            if (!clipUserFields) {
                return true;
            }
        } catch (JSONException e) {
            return true;
        }
        return false;
    }
}
